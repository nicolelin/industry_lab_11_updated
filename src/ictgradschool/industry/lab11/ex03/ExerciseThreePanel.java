package ictgradschool.industry.lab11.ex03;

import javax.swing.*;
import java.awt.*;

/**
 * A JPanel that draws some houses using a Graphics object.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseThreePanel extends JPanel {

    /** All outlines should be drawn this color. */
    private static final Color OUTLINE_COLOR = Color.black;

    /** The main "square" of the house should be drawn this color. */
    private static final Color MAIN_COLOR = new Color(255, 229, 204);

    /** The door should be drawn this color. */
    private static final Color DOOR_COLOR = new Color(150, 70, 20);

    /** The windows should be drawn this color. */
    private static final Color WINDOW_COLOR = new Color(255, 255, 153);

    /** The roof should be drawn this color. */
    private static final Color ROOF_COLOR = new Color(255, 153, 51);

    /** The chimney should be drawn this color. */
    private static final Color CHIMNEY_COLOR = new Color(153, 0, 0);

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseThreePanel() {
        setBackground(Color.white);
    }

    /**
     * Draws eight houses, using the method that you implement for this exercise.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawHouse(g, 125, 177, 3);
        drawHouse(g, 199, 193, 7);
        drawHouse(g, 292, 55, 5);
        drawHouse(g, 29, 110, 8);
        drawHouse(g, 379, 386, 7);
        drawHouse(g, 127, 350, 12);
        drawHouse(g, 289, 28, 2);
        drawHouse(g, 300, 150, 16);
    }

    /**
     * Draws a single house, with its top-left at the given coordinates, and with the given size multiplier.
     *
     * @param g the {@link Graphics} object to use for drawing
     * @param left the x coordinate of the house's left side
     * @param top the y coordinate of the top of the house's roof
     * @param size the size multipler. If 1, the house should be drawn as shown in the grid in the lab handout.
     */
    private void drawHouse(Graphics g, int left, int top, int size) {

        // TODO Draw a house, as shown in the lab handout.

        // House main
        g.setColor(MAIN_COLOR);
        g.fillRect(left+(size*0),top+(size*5),size*10,size*7);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(left+(size*0),top+(size*5),size*10,size*7);

        // House door
        g.setColor(DOOR_COLOR);
        g.fillRect(left+(size*4),top+(size*8),size*2,size*4);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(left+(size*4),top+(size*8),size*2,size*4);

        // House window left glass
        g.setColor(WINDOW_COLOR);
        g.fillRect(left+(size*1),top+(size*7),size*2,size*2);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(left+(size*1),top+(size*7),size*2,size*2);

        // House window left frames
        // Horizontal
        g.setColor(OUTLINE_COLOR);
        g.drawLine(left+(size*1),top+(size*8),left+(size*3),top+(size*8));
        // Vertical
        g.setColor(OUTLINE_COLOR);
        g.drawLine(left+(size*2),top+(size*7),left+(size*2),top+(size*9));

        // House window right glass
        g.setColor(WINDOW_COLOR);
        g.fillRect(left+(size*7),top+(size*7),size*2,size*2);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(left+(size*7),top+(size*7),size*2,size*2);

        // House window right frames
        // Horizontal
        g.setColor(OUTLINE_COLOR);
        g.drawLine(left+(size*7),top+(size*8),left+(size*9),top+(size*8));
        // Vertical
        g.setColor(OUTLINE_COLOR);
        g.drawLine(left+(size*8),top+(size*7),left+(size*8),top+(size*9));

        // House roof
        // Polygon
        int roofXpoints[] = {left+(size*0),left+(size*5),left+(size*10)};
        int roofYpoints[] = {top+(size*5),top+(size*0),top+(size*5)};
        int roofNpoints = 3;

        g.setColor(ROOF_COLOR);
        g.fillPolygon(roofXpoints, roofYpoints, roofNpoints);

        g.setColor(OUTLINE_COLOR);
        g.drawPolygon(roofXpoints, roofYpoints, roofNpoints);

        // House chimney
        // Polygon
        int chimneyXpoints[] = {left+(size*7),left+(size*8),left+(size*8),left+(size*7),left+(size*7)};
        int chimneyYpoints[] = {top+(size*1),top+(size*1),top+(size*3),top+(size*2),top+(size*1)};
        int chimneyNpoints = 5;

        g.setColor(CHIMNEY_COLOR);
        g.fillPolygon(chimneyXpoints,chimneyYpoints,chimneyNpoints);

        g.setColor(OUTLINE_COLOR);
        g.drawPolygon(chimneyXpoints,chimneyYpoints,chimneyNpoints);

        // Other ways of drawing the chimney
        // Draw a rectangle layered behind other objects
        // Draw polygons using new Polygon object ... define a variable aShape = new Polygon, then add the coordinates

    }
}