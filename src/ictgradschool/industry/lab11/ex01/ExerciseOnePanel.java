package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeight;
    private JTextField heightInput;
    private JTextField weightInput;
    private JTextField calculatedBMI;
    private JTextField calculatedMaxHealthyWeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can also access them.
        calculateBMIButton = new JButton("Calculate BMI");
        calculateHealthyWeight = new JButton("Calculate Healthy Weight");
        heightInput = new JTextField(15);
        weightInput = new JTextField(15);
        calculatedBMI = new JTextField(15);
        calculatedMaxHealthyWeight = new JTextField(15);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as local variables, rather than instance variables.
        JLabel label1 = new JLabel("Height in metres: ");
        JLabel label2 = new JLabel("Weight in kilograms: ");
        JLabel label3 = new JLabel("Your Body Mass Index (BMI) is: ");
        JLabel label4 = new JLabel("Maximum Healthy Weight for your Height: ");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(label1);
        this.add(heightInput);

        this.add(label2);
        this.add(weightInput);

        this.add(calculateBMIButton);
        this.add(label3);
        this.add(calculatedBMI);
        calculatedBMI.setEditable(false);

        this.add(calculateHealthyWeight);
        this.add(label4);
        this.add(calculatedMaxHealthyWeight);
        calculatedMaxHealthyWeight.setEditable(false);

        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHealthyWeight.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        double userHeight = Double.parseDouble(heightInput.getText());
        double userWeight = Double.parseDouble(weightInput.getText());

        if (event.getSource() == calculateBMIButton) {
            calculatedBMI.setText(roundTo2DecimalPlaces(userWeight / (userHeight * userHeight)) + " BMI");
        }
        else if (event.getSource() == calculateHealthyWeight) {
            calculatedMaxHealthyWeight.setText(roundTo2DecimalPlaces(24.9 * userHeight * userHeight) + " KGs");
        }
    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}